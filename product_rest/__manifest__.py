# -*- coding: utf-8 -*-

{
    'name': 'Product Rest API',
    'category': 'Website/Website',
    'sequence': 200,
    'summary': 'This API will return product details such as name, sale price, cost, on-hand quantity, category',
    'website': '',
    'description': "",
    'author': 'Ghanshyam Prajapati',
    'website': "",
    'maintainer': 'Ghanshyam Prajapati',
    'depends': [ 'product', 'stock'],
    'data': [
    ],
    'demo': [
    ],
    'application': True,
}
