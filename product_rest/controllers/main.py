from odoo import http
from odoo.http import request


class APIController(http.Controller):

    @http.route('/get_product_data', auth='public', type='json', csrf=False)
    def detailed_product(self, product_id=False, **kwargs):
        product_data = {}
        if product_id:
            print ("Product::",product_id)
            
            product = request.env['product.product'].sudo().browse(product_id)
            print ("product",product)
            try:
                product_data = {
                    'name': product.name,
                    'sale_price': product.list_price,
                    'cost': product.standard_price,
                    'on_hand_qty': product.qty_available,
                    'category': product.categ_id and product.categ_id.name or ''
                }
#             PRODUCT_FIELDS = ['name', 'list_price', 'standard_price', 'qty_available', 'categ_id']
#             product_data = product.with_context(lang=request.env.user.lang).read(PRODUCT_FIELDS)[0]
            except:
                product_data = {'Product not found'}


        return {'status': True, 'data': product_data}
