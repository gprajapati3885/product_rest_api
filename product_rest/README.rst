Product Rest API
================

This module enables product api which will return product details like name, sale price, cost, on-hand quantity and category.

Usage
=====
For testing in Postman, Open Postman client, Click on New Collections >> Add requests.
For more details please check screenshots from static/description.

Please use db-filter option to run this module smoothly.

Configuration
=============


Known issues / Roadmap
======================

* ...

Bug Tracker
===========

Contact prajapati.ghanshyam@gmail.com


Contributors
------------

* Ghanshyam Prajapati <prajapati.ghanshyam@gmail.com>

Maintainer
----------

This module is maintained by Ghanshyam Prajapati.
